package hu.oe.WarehouseApplication.controller;

import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.transformer.BoxesListViewToBoxes;
import hu.oe.WarehouseApplication.transformer.BoxesToAddFormViewAndBack;
import hu.oe.WarehouseApplication.webdomain.BoxForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AddBoxController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddBoxController.class);

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private BoxesListViewToBoxes boxesListViewToBoxes;

    @Autowired
    private BoxesToAddFormViewAndBack BoxesToAddFormViewAndBack;

    /*
    @PostMapping("addbox")
    public String addBox(AddBoxRequest addBoxRequest)
    {

        Box newBox = new Box();
        newBox = boxesListViewToBoxes.transformAddBoxRequest(addBoxRequest);

        warehouseService.saveBox(newBox);

        LOGGER.info("Box is successfully added.");

        return  "redirect:boxes";
    }*/
/*
    @GetMapping("/boxes")
    public  String addNewBow(Model model)
    {
        AddBoxForm addBoxForm = BoxesToAddFormView.transformUsersBoxesToAddBoxFormView(warehouseService.getUserById(1L));
        model.addAttribute("addboxform", addBoxForm);
        return "redirect:boxes";
    }
*/
    @PostMapping("boxes")
    public String addBox(BoxForm boxForm){

        Box box = new Box();
        box = BoxesToAddFormViewAndBack.transformBoxFormatToBox(boxForm);

        warehouseService.saveBox(box);
        LOGGER.info("Box [id: {}] is successfully added.", box.getBoxId());

        return "redirect:boxes";
    }




}
