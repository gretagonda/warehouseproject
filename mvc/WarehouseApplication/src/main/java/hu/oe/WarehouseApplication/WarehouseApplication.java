package hu.oe.WarehouseApplication;

import hu.oe.WarehouseApplication.service.TestDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarehouseApplication implements CommandLineRunner {


	@Autowired
	private App app;

	@Autowired
	private TestDataGenerator testDataGenerator;

	public static void main(String[] args) {
		SpringApplication.run(WarehouseApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		//testDataGenerator.createTestData();
		app.start();
	}
}
