package hu.oe.WarehouseApplication.persistance;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Stream;

public enum Category {
    FLAMMABLE,
    LIGHT,
    HEAVY,
    FRAGILE;


}
