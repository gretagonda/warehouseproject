package hu.oe.WarehouseApplication.transformer;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;
import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.webdomain.BoxListView;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BoxesToListView {

    public BoxesToListView() {
    }

    public List<BoxListView> transformBoxesToListView(Iterable<Box> boxes )
    {
        List<BoxListView> boxList = new ArrayList<>();

        for(Box box : boxes)
        {
            Long id = box.getBoxId();
            Long storageroomId = box.getStorageRoomId();
            int x = box.getSize().getX();
            int y = box.getSize().getY();
            int sum = x*y;
            String size = sum+" nm";
            String materials = "";
            List<Material> materialList = box.getMaterials();
            if(materialList.size()!=0) {
                for (int i = 0; i < materialList.size() - 1; i++) {
                    materials = materials + materialList.get(i) + ", ";
                }

                materials = materials + materialList.get(materialList.size() - 1);
            }

            String categories = "";
            List<Category> categoryList = box.getCategories();

            if(categoryList.size()!=0) {
                for (int i = 0; i < categoryList.size() - 1; i++) {
                    categories = categories + categoryList.get(i) + ", ";
                }

                categories = categories + categoryList.get(categoryList.size() - 1);
            }

                BoxListView boxView = new BoxListView(id, storageroomId, size, materials, categories);
                boxList.add(boxView);


        }

        return boxList;
    }

}
