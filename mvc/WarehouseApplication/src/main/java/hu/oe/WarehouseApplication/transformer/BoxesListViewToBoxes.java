package hu.oe.WarehouseApplication.transformer;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;
import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.repository.CostumerRepository;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.webdomain.AddBoxRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BoxesListViewToBoxes {

    WarehouseService warehouseService;

    CostumerRepository costumerRepository;

    @Autowired
    public BoxesListViewToBoxes(WarehouseService warehouseService,CostumerRepository costumerRepository ) {
        this.warehouseService = warehouseService;
        this.costumerRepository = costumerRepository;
    }

    public  Box transformAddBoxRequest(AddBoxRequest addBoxRequest)
    {
        Box newBox = new Box();
        newBox.setStorageRoom(warehouseService.getStorageRoomById(Long.parseLong(addBoxRequest.getStorageroom_id())));
        String size = addBoxRequest.getSize();
        String[] sizeArray = size.split("x");
        int x=Integer.parseInt(sizeArray[0]);
        int y=Integer.parseInt(sizeArray[1]);
        newBox.setSize2(x,y);

        newBox.setCostumer(costumerRepository.findById(new Long(1)).get());

        List<Material> material = new ArrayList<>();
        material.add(Material.valueOf(addBoxRequest.getMaterials().toUpperCase()));
        newBox.setMaterials(material);

        List<Category> category = new ArrayList<>();
        category.add(Category.valueOf(addBoxRequest.getCategories().toUpperCase()));
        newBox.setCategories(category);

        return newBox;
    }
}
