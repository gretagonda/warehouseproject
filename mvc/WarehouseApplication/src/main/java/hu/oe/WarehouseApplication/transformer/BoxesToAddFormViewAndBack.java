package hu.oe.WarehouseApplication.transformer;

import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.webdomain.BoxForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BoxesToAddFormViewAndBack {

    WarehouseService warehouseService;


    @Autowired
    public BoxesToAddFormViewAndBack(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    public BoxForm transformUsersBoxesToAddBoxFormView(Costumer activeUser)
    {
        BoxForm boxForm = new BoxForm();
        boxForm.setStorageroomid(warehouseService.getStorageRoomIdsAsArray(1L));
        boxForm.setSize("");
        return  boxForm;
    }

    public Box transformBoxFormatToBox(BoxForm boxForm)
    {
        Box box = new Box();
        //box.setCostumer(warehouseService.getLoggedInCostumer());
        box.setCostumer(warehouseService.getUserById(1L));
        String[] xy = boxForm.getSize().split("x");
        int x = Integer.parseInt(xy[0]);
        int y = Integer.parseInt(xy[1]);
        box.setSize2(x,y);
        box.setStorageRoom(warehouseService.getStorageRoomById(Long.parseLong(boxForm.getStorageroomid().stream().findFirst().get())));
        box.setMaterials(boxForm.getMaterials());
        box.setCategories(boxForm.getCategories());

        return  box;
    }
}
