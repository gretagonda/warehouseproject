package hu.oe.WarehouseApplication.view;


import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import hu.oe.WarehouseApplication.persistance.Warehouse;


public interface View {
    public void printWelcomeMessage();
    public String readUsername();
    public String readPassword();
    public String getInput();
    public void printAuthenticatedMessage(String username);
    public String printLogOutConfirmationMessage();
    public void printLogOutMessage(String username);
    public void invalidChoiceMessage();


    public void printMainMenu();
    public void printStorageRoomsMenu();
    public void printBoxesMenu();

    public void printWarehouseStorageRooms(Warehouse warehouse);
    public void printStorageRoomsRentByCostumer(Costumer costumer);
    public void printCostumerBoxes(Costumer costumer);
    public String selectBoxToRemove(Costumer costumer);
    public void printCostumerStorageRoomsWithSizes(Costumer costumer);

    public void printIncorrectCredentialsMessage();
    public void printNoRoomMessage();
    public void printRentStorageRoomPrompt();
    public void printCancelStorageRoomPrompt();
    public void printReadStorageRoomPrompt();
    public Box readBox();
    public void printException(Exception e);
    public void printStorageRoomSuccesfullyRented(Long id);
    public void printStorageRoomSuccesfullyUnRented(Long id);
    public void printNewBoxIsRented(Long boxId, Long roomId);
    public void notEnoughRoomMessage();
    public void deletedBoxMessage(Long id);



}
