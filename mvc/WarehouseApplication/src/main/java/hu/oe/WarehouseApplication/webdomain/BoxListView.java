package hu.oe.WarehouseApplication.webdomain;

public class BoxListView {

    private final Long id;
    private final Long storageroom_id;
    private final String size;
    private final String materials;
    private final String categories;

    public BoxListView(Long id, Long storageroom_id, String size, String materials, String categories) {
        this.id = id;
        this.storageroom_id = storageroom_id;
        this.size = size;
        this.materials = materials;
        this.categories = categories;
    }

    public Long getId() {
        return id;
    }

    public Long getStorageroom_id() {
        return storageroom_id;
    }

    public String getSize() {
        return size;
    }

    public String getMaterials() {
        return materials;
    }

    public String getCategories() {
        return categories;
    }
}
