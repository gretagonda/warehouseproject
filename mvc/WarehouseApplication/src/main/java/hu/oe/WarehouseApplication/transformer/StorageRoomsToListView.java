package hu.oe.WarehouseApplication.transformer;

import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.webdomain.StorageRoomListView;

import java.util.ArrayList;
import java.util.List;

public class StorageRoomsToListView {


    public StorageRoomsToListView()
    {

    }


    public List<StorageRoomListView> transformStorageRoomsToViewList(Iterable<StorageRoom> storageRooms)
    {
        List<StorageRoomListView> storageRoomList = new ArrayList<>();

        for(StorageRoom room : storageRooms)
        {
            Long storageRoom_id=room.getStorageRoomId();
            int x = room.getSize().getX();
            int y = room.getSize().getY();
            int sum = x*y;
            String size = sum+" nm";
            String owner ="";
            if(!room.isFree()) {
                owner = room.getCostumer().getUsername();
            }
            boolean isFree = room.isFree();
            int numBoxes = room.getBoxes().size();
            StorageRoomListView storageRoomView = new StorageRoomListView(storageRoom_id, size, owner, isFree, numBoxes);
            storageRoomList.add(storageRoomView);
        }

        return  storageRoomList;
    }

}
