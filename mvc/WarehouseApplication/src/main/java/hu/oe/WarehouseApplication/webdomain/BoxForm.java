package hu.oe.WarehouseApplication.webdomain;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;

import java.util.ArrayList;
import java.util.List;

public class BoxForm {

    private List<String> storageroomid;
    private String size;
    private List<Material> materials;
    private List<Category> categories;

    public BoxForm() {
        materials = new ArrayList<>();
        categories = new ArrayList<>();
    }


    public List<String> getStorageroomid() {
        return storageroomid;
    }

    public void setStorageroomid(List<String> storageroomid) {
        this.storageroomid = storageroomid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    public List<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
