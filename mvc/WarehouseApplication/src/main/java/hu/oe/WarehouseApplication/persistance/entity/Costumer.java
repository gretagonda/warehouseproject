package hu.oe.WarehouseApplication.persistance.entity;

import lombok.AllArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Costumer {

    @OneToMany(mappedBy="costumer", fetch = FetchType.EAGER)
    @ElementCollection
    private List<StorageRoom> storageRooms;
    @Id
    @GeneratedValue
    private Long costumerId;
    private String username;
    private String password;

    public Costumer()
    {
        storageRooms = new ArrayList<StorageRoom>();
    }

    public List<StorageRoom> getStorageRooms() {
        return storageRooms;
    }

    public void setStorageRooms(List<StorageRoom> storageRooms) {
        this.storageRooms = storageRooms;
    }

    public Long getCostumerId() {
        return costumerId;
    }

    public void setCostumerId(Long costumerId) {
        this.costumerId = costumerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addStorageRoom(StorageRoom storageroom)
    {
        storageRooms.add(storageroom);
    }

    public void removeStorageRoom(StorageRoom storageroom)
    {
        storageRooms.remove(storageroom);
    }

    public void removeStorageRoomById(Long storageroom)
    {
        StorageRoom sr = new StorageRoom();
        for(StorageRoom room : storageRooms)
        {
            if(room.getStorageRoomId().equals(storageroom))
            {
                sr = room;
            }
        }
        storageRooms.remove(sr);
    }

}
