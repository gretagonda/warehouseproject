package hu.oe.WarehouseApplication.persistance.repository;

import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CostumerRepository extends CrudRepository<Costumer, Long> {

     Iterable<Costumer> findAll();
     Optional<Costumer> findById(Long id);

}
