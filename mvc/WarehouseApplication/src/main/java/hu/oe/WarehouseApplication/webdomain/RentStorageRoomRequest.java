package hu.oe.WarehouseApplication.webdomain;

public class RentStorageRoomRequest {

    private String storageRoomId;

    public String getStorageRoomId() {
        return storageRoomId;
    }

    public void setStorageRoomId(String storageRoomId) {
        this.storageRoomId = storageRoomId;
    }
}
