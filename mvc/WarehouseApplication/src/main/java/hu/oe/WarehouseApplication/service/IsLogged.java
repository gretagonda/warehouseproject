package hu.oe.WarehouseApplication.service;


import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import org.springframework.stereotype.Component;

@Component
public class IsLogged {
    private boolean isLoggedIn;
    private Costumer costumer;
    public IsLogged()
    {
        isLoggedIn = false;
        setCostumer(new Costumer());
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }
}
