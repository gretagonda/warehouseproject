package hu.oe.WarehouseApplication.persistance.repository;

import hu.oe.WarehouseApplication.persistance.entity.Box;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BoxRepository extends CrudRepository<Box, Long> {

    Optional<Box> findById(Long id);
}
