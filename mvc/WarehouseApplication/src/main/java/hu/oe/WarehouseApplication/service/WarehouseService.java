package hu.oe.WarehouseApplication.service;


import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.persistance.repository.BoxRepository;
import hu.oe.WarehouseApplication.persistance.repository.CostumerRepository;
import hu.oe.WarehouseApplication.persistance.repository.StorageRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@org.springframework.stereotype.Service
public class WarehouseService implements  Service{

    private final BoxRepository boxRepository;
    private final CostumerRepository costumerRepository;
    private final StorageRoomRepository storageRoomRepository;
    private IsLogged isLogged;

    @Autowired
    public WarehouseService(BoxRepository boxRepository, CostumerRepository costumerRepository, StorageRoomRepository storageRoomRepository, IsLogged isLogged)
    {
        this.boxRepository = boxRepository;
        this.costumerRepository = costumerRepository;
        this.storageRoomRepository = storageRoomRepository;
        this.isLogged = isLogged;
    }

    public IsLogged getIsLogged() {
        return isLogged;
    }

    public void setIsLogged(IsLogged isLogged) {
        this.isLogged = isLogged;
    }

    public Iterable<StorageRoom> getAllStorageRooms()
    {
        return  storageRoomRepository.findAll();
    }

    public Iterable<StorageRoom> getMyStorageRooms(Costumer costumer)
    {
       return  storageRoomRepository.findAllByCostumer(costumer);
    }

    public Iterable<StorageRoom> getUserStorageRooms(Long id)
    {
        return getMyStorageRooms(costumerRepository.findById(id).get());
    }

    public  Costumer getUserById(Long id)
    {
        return costumerRepository.findById(id).get();
    }

    public List<String> getStorageRoomIdsAsArray(Long id)
    {
        List<String> ids = new ArrayList<>();

        Iterable<StorageRoom> usersRooms = getUserStorageRooms(id);

        for(StorageRoom room : usersRooms)
        {
            ids.add(Long.toString(room.getStorageRoomId()));
        }

        return  ids;
    }
    public List<Box> getCostumersBoxes(Costumer costumer)
    {
        List<Box> boxes = new ArrayList<>();
        Iterable<Box> allBoxes = boxRepository.findAll();
        for(Box box: allBoxes)
        {
            if(!box.getCostumerId().equals(0L) && box.getCostumerId().equals(costumer.getCostumerId()))
            {
                boxes.add(box);
            }
        }

        return  boxes;
    }


    public  List<Box> getBoxesByCostumer(Long id)
    {
        return getCostumersBoxes(costumerRepository.findById(id).get());
    }
    public void getStorageRoomsToCostumers()
    {
        Iterable<Costumer> costumers = costumerRepository.findAll();
        Iterable<StorageRoom> storageRooms = storageRoomRepository.findAll();

        for(Costumer c : costumers)
        {
            for(StorageRoom sr : storageRooms)
            {
                if(sr.getCostumer()!=null) {
                    if (c.getCostumerId().equals(sr.getCostumerId())) {
                        isLogged.getCostumer().addStorageRoom(sr);
                        Costumer cost = costumerRepository.findById(c.getCostumerId()).get();
                        cost.addStorageRoom(sr);
                        costumerRepository.save(cost);
                    }
                }
            }
        }

    }

    @Override
    public boolean authenticate(String username, String password) {

        Iterable<Costumer> users = costumerRepository.findAll();
        for(Costumer user : users) {
                if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                    isLogged.setLoggedIn(true);
                    isLogged.setCostumer(user);
                    return true;
                }
        }

        return false;
    }

    @Override
    public boolean isLoggedIn() {
        if(isLogged.isLoggedIn()==true)
        {
            return true;
        }
        return false;
    }

    @Override
    public Costumer getLoggedInCostumer() {
        if(isLogged.isLoggedIn()==true)
        {
            return isLogged.getCostumer();
        }
        return null;
    }

    public Costumer getLoggedInCostumerFromDatabase() {
        if(isLogged.isLoggedIn()==true)
        {
            Costumer costumer = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();

            return costumer;
        }
        return null;
    }

    @Override
    public void logout() throws IOException {
        //this.saveData();
        isLogged.setLoggedIn(false);

    }

    @Override
    public void rentStorageRoom(Long storageRoomId) {
        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        sr.setFree(false);
        //sr.setCostumer(isLogged.getCostumer());

        sr.setCostumer(costumerRepository.findById(new Long(1)).get());
        storageRoomRepository.save(sr);
        //isLogged.getCostumer().addStorageRoom(sr);
        //Costumer cst = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();
        Costumer cost = costumerRepository.findById(new Long(1)).get();
        cost.addStorageRoom(sr);
        costumerRepository.save(cost);

    }

    @Override
    public void cancelStorageRoomRenting(Long storageRoomId) {
        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        //isLogged.getCostumer().removeStorageRoomById(storageRoomId);
        Costumer c = costumerRepository.findById(new Long(1)).get();
        c.removeStorageRoomById(storageRoomId);
        //Costumer cst = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();
        //cst.removeStorageRoomById(storageRoomId);
        sr.setFree(true);
        sr.setCostumer(null);
        storageRoomRepository.save(sr);
        costumerRepository.save(c);

    }

    @Transactional
    @Override
    public void storeRoom(Box box, Long storageRoomId) {


        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        sr.addBox(box);
        boxRepository.save(box);
        storageRoomRepository.save(sr);
        for(StorageRoom room : isLogged.getCostumer().getStorageRooms())
        {
            if(room.getStorageRoomId().equals(storageRoomId))
            {
                room.addBox(box);
            }
        }
    }

    public void saveBox(Box box)
    {
        boxRepository.save(box);
    }

    @Override
    public void removeBox(Long boxId) {
        Box bx = boxRepository.findById(boxId).get();
        boxRepository.delete(bx);

        Costumer c = costumerRepository.findById(new Long(1)).get();

        for(int i = 0; i < c.getStorageRooms().size(); i++)
        {
            for(int j = 0; j < c.getStorageRooms().get(i).getBoxes().size(); j++)
            {
                Box b = c.getStorageRooms().get(i).getBoxes().get(j);
                if(b.getBoxId().equals(boxId))
                {
                    c.getStorageRooms().get(i).removeBox(b);
                }
            }
        }
/*
        for(int i = 0; i < isLogged.getCostumer().getStorageRooms().size(); i++)
        {
            for(int j = 0; j < isLogged.getCostumer().getStorageRooms().get(i).getBoxes().size(); j++)
            {
                Box b = isLogged.getCostumer().getStorageRooms().get(i).getBoxes().get(j);
                if(b.getBoxId().equals(boxId))
                {
                    isLogged.getCostumer().getStorageRooms().get(i).removeBox(b);
                }
            }
        }*/

    }

    @Override
    public boolean isStorageRoomRentable(Long id) {

        StorageRoom storageRoom = storageRoomRepository.findById(id).get();
        if(storageRoom.isFree())
        {
                return true;
        }else {

            return false;
        }
    }

    public  StorageRoom getStorageRoomById(Long id)
    {
        return storageRoomRepository.findById(id).get();
    }

}
