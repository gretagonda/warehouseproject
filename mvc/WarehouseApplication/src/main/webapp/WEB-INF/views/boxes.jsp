<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/storageroom.css">
</head>
<body>
    <div>
    <a href="storagerooms">
        <button>Storage rooms</button>
    </a>
    <a href="mystoragerooms">
        <button>My Storage rooms</button>
    </a>
    <a href="boxes">
    <button>My boxes</button>
    </a>
    </div>
    <br><br>



    <table>
    <tr>
        <th>Id</th>
        <th>Storage room id</th>
        <th>Size</th>
        <th>Materials</th>
        <th>Categories</th>
    </tr>
    <c:forEach items="${boxes}" var="box">
    <tr>
        <td>${box.id}</td>
        <td>${box.storageroom_id}</td>
        <td>${box.size}</td>
        <td>${box.materials}</td>
        <td>${box.categories}</td>
        <td>

        <form action="/removebox" method="post">
                <input type="text" style="display:none" id ="boxId" name="boxId" value="${box.id}">
                <input type="submit" value="Remove box">
        </form>

        </td>
    </tr>
    </c:forEach>
    </table>
    <br><br>


    <form:form modelAttribute="addboxform" action="/boxes" method="post">
        <div>
            <form:label path="storageroomid">Storage Room Id</form:label>
            <form:select path="storageroomid">
                <form:option value="" label="-- select storage room by id"/>
                <form:options items="${storagerooms}"/>
            </form:select>
        </div>
        <div>
            <form:label path="size">Size</form:label>
            <form:input path="size"/>
        </div>
        <div>
            <form:label path="materials">Materials</form:label>
            <form:select size="4" path="materials">
                <form:option value="" label=""/>
                <form:options items="${materials}"/>
            </form:select>
        </div>
        <div>
            <form:label path="categories">Categories</form:label>
            <form:select size="4" path="categories">
                <form:option value="" label=""/>
                <form:options items="${categories}"/>
            </form:select>
        </div>
        <div>
            <input type="submit" value="Save"/>
        </div>
    </form:form>

</body>
</html>
