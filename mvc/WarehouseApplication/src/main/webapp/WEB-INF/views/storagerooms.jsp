<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/storageroom.css">
</head>
<body>
    <div>
    <a href="storagerooms">
        <button>Storage rooms</button>
    </a>
    <a href="mystoragerooms">
        <button>My Storage rooms</button>
    </a>
    <a href="boxes">
    <button>My boxes</button>
    </a>
    </div>
    <br><br>


    <table>
    <tr>
        <th>Id</th>
        <th>Size</th>
        <th>Owner</th>
        <th>Is free</th>
        <th>Number of boxes</th>
    </tr>
    <c:forEach items="${storagerooms}" var="storageroom">
    <tr>
        <td>${storageroom.storageRoom_id}</td>
        <td>${storageroom.size}</td>
        <td>${storageroom.owner}</td>
        <td>${storageroom.isFree}</td>
        <td>${storageroom.numBoxes}</td>
    </tr>
    </c:forEach>
    </table>
    <br><br>
    <form action="/rentStorageRoom" method="post">
        <label for="storageRoomId">StorageRoom Id:</label><br>
        <input type="text" id ="storageRoomId" name="storageRoomId">
        <input type="submit" value="Rent StorageRoom">
    </form>
</body>
</html>
