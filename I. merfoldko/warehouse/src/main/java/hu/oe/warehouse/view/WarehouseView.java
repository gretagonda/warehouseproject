package hu.oe.warehouse.view;

import hu.oe.warehouse.domain.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WarehouseView implements View{
    private Warehouse warehouse;

    public WarehouseView(Warehouse warehouse)
    {
        this.warehouse = warehouse;
    }

    @Override
    public void printWelcomeMessage() {
        System.out.println("-- Welcome to the Warehouse Application! --");

    }

    @Override
    public String readUsername() {
        System.out.println("Give me your username: ");
        String username = getInput();
        return username;

    }

    @Override
    public String readPassword() {
        System.out.println("Give me your password: ");
        String password = getInput();
        return password;
    }

    @Override
    public String getInput() {
        String input = "";
        //String input = System.console().readLine();
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));
        try {
            input = reader.readLine();
        }catch(IOException e)
        {
        }
        return input;
    }

    @Override
    public void printAuthenticatedMessage(String username){
        System.out.println("--"+username+" costumer logged in --");
    }

    @Override
    public void printMainMenu() {
        System.out.println("-- Main menu --");
        System.out.println("1: Storage rooms.");
        System.out.println("2: Boxes.");
        System.out.println("3: Quit.");


    }

    @Override
    public void printStorageRoomsMenu() {
        System.out.println("-- Storage Rooms menu --");
        System.out.println("1: Get information about all storage rooms.");
        System.out.println("2: Rent a new storage rooms.");
        System.out.println("3: Cancel renting.");
        System.out.println("4: Get information about my storage rooms.");
        System.out.println("Q: Go back!");

    }

    @Override
    public void printBoxesMenu() {
        System.out.println("-- Boxes menu --");
        System.out.println("1: New box");
        System.out.println("2: Remove box");
        System.out.println("3: List my boxes");
        System.out.println("Q: Go back!");

    }

    @Override
    public void printWarehouseStorageRooms() {
        for(StorageRoom room : warehouse.getStorageRooms())
        {
            System.out.println("      -- Storage room --");
            System.out.println("Storage room ID:    "+room.getId());
            System.out.println("Size:               "+room.getSize().getX()+"x"+room.getSize().getY());
            System.out.println("Is free?:           "+room.isFree());
            System.out.println("Number of boxes:    "+room.getBoxes().size());

        }
        System.out.println("Q: Go back!");
    }

    @Override
    public void printStorageRoomsRentByCostumer(Costumer costumer) {
        for(StorageRoom room : warehouse.getStorageRooms())
        {
            if(room.getOwnerId().equals(costumer.getId())) {

                System.out.println("      -- Storage room --");
                String id = room.getId().toString();
                System.out.println("Storage room ID:    " + id);
                String x = String.valueOf(room.getSize().getX());
                String y = String.valueOf(room.getSize().getY());
                System.out.println("Size:               " + x + "x" + y);
                boolean free = room.isFree();
                System.out.println("Is free?:           " + free);
                String size = String.valueOf(room.getBoxes().size());
                System.out.println("Number of boxes:    " + size);
            }

        }
        System.out.println("Q: Go back!");

    }

    @Override
    public void printCostumerBoxes(Costumer costumer) {
        for(StorageRoom room : costumer.getStorageRooms())
        {
            for(Box box: room.getBoxes())
            {
                System.out.println("      -- Box "+box.getId()+"--");
                System.out.println("Size:      "+box.getSize().getX()+"x"+box.getSize().getY());
                System.out.print("Materials:   ");
                for(Material mat:box.getMaterials()) {
                    System.out.print("["+mat+"]");
                }
                System.out.print("\n");
                System.out.print("Categories:  ");
                for(Category cat:box.getCategories()) {
                    System.out.print("["+cat+"]");
                }
                System.out.print("\n");
            }

        }
        System.out.println("Q: Go back!");


    }

    @Override
    public String selectBoxToRemove(Costumer costumer) {
        for(StorageRoom room : costumer.getStorageRooms())
        {
            for(Box box: room.getBoxes())
            {
                System.out.println("      -- Box "+box.getId()+"--");
                System.out.println("Size:      "+box.getSize().getX()+"x"+box.getSize().getY());
                System.out.print("Materials:   ");
                for(Material mat:box.getMaterials()) {
                    System.out.print("["+mat+"]");
                }
                System.out.print("\n");
                System.out.print("Categories:  ");
                for(Category cat:box.getCategories()) {
                    System.out.print("["+cat+"]");
                }
                System.out.print("\n");
            }

        }
        System.out.println("Go back!");

        System.out.println("Give the ID of the box you want to remove from the storage room:");
        String toRemove = getInput().toUpperCase();
        return toRemove;
    }

    @Override
    public void printIncorrectCredentialsMessage() {
        System.out.println("Incorrect username or password. Please try again!");

    }

    @Override
    public void printNoRoomMessage() {
        System.out.println("-- There are no existing storage rooms assigned to the logged in user. --");

    }

    @Override
    public void printRentStorageRoomPrompt() {
        System.out.println("Enter the ID of the storage room you want to rent!");

    }

    @Override
    public void printCancelStorageRoomPrompt() {
        System.out.println("Enter the ID of the storage room you want to stop renting!");

    }

    @Override
    public void printNewBoxIsRented(Long boxId, Long roomId) {
        System.out.println("-- Box with ID: "+ boxId + " stored in storage room with ID: " + roomId);

    }

    @Override
    public void notEnoughRoomMessage() {
        System.out.println("Not enough room in the room to add this box!");

    }

    @Override
    public void printReadStorageRoomPrompt() {
        System.out.println("Give the ID of the storage room you want to place the box:");

    }

    @Override
    public void printStorageRoomSuccesfullyRented(Long id) {
        System.out.println("-- Storage room with id: "+id+" rented! --");

    }
    @Override
    public void printStorageRoomSuccesfullyUnRented(Long id) {
        System.out.println("-- Storage room with id: "+id+" is no longer rented! --");

    }

    @Override
    public Box readBox(Long costumerId, Long storageRoomId) {
        Box box = new Box();
        Box.setCounter(1);
        box.setId(Long.valueOf(Box.getCounter()));
        box.setCostumerId(costumerId);
        box.setStorageRoomId(storageRoomId);
        System.out.println("Give the size of the box:");
        String size = this.getInput();
        String[] sizeArray = size.split("x");
        int x = Integer.parseInt(sizeArray[0]);
        int y = Integer.parseInt(sizeArray[1]);
        Size sizeSize = new Size();
        sizeSize.setX(x);
        sizeSize.setY(y);
        box.setSize(sizeSize);
        System.out.println("Give the content of the box:");
        String content = this.getInput().toUpperCase();
        String[] contentArray = content.split(",");
        for(String c : contentArray)
        {
            box.addMaterials(Material.valueOf(c.trim()));
        }
        System.out.println("Give the categories of the content:");
        String contentCategories = this.getInput().toUpperCase();
        String[] contentCategoryArray = contentCategories.split(",");
        for(String c : contentCategoryArray)
        {
            box.addCategories(Category.valueOf(c.trim()));
        }

        return box;
    }

    @Override
    public String printLogOutConfirmationMessage() {
        System.out.println("Are you sure?");
        String choice = this.getInput();
        return choice.toUpperCase();

    }

    @Override
    public void printLogOutMessage(String username) {
        System.out.println("-- " +username+" user logged out --");

    }

    @Override
    public void invalidChoiceMessage() {
        System.out.println("The choice you give is invalid. Please give an other one!");

    }

    @Override
    public void printCostumerStorageRoomsWithSizes(Costumer costumer) {
        for(StorageRoom room: costumer.getStorageRooms())
        {
            System.out.println("      -- Storage room "+room.getId()+" --");
            int roomArea = (room.getSize().getX())*(room.getSize().getY());
            System.out.println("Size of the room:         "+roomArea+"m^2");
            int sumBoxesArea=0;
            for(Box box:room.getBoxes())
            {
                int boxArea = (box.getSize().getX())*(box.getSize().getY());
                sumBoxesArea= sumBoxesArea+boxArea;
            }
            System.out.println("Size of all boxes:        "+sumBoxesArea+"m^2");
            int areaLeft=roomArea-sumBoxesArea;
            System.out.println("Size of vacant area left: "+areaLeft+"m^2");
        }

    }

    @Override
    public void printException(Exception e) {
        System.out.println("Exception thrown  :" + e);
    }

}
