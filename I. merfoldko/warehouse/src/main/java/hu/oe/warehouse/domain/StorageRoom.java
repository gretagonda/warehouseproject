package hu.oe.warehouse.domain;
import java.util.ArrayList;
import java.util.List;


public class StorageRoom {
    private Long id;
    private boolean isFree;
    private Long ownerId;
    private List<Box> boxes;
    private Size size;

    public StorageRoom()
    {
        ownerId = 0L;
        isFree = true;
        boxes=new ArrayList<Box>();
    }


    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean isFree) {
        this.isFree = isFree;
    }


    public Long getOwnerId() {
        return ownerId;
    }


    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }


    public List<Box> getBoxes() {
        return boxes;
    }


    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public void addBox(Box box) {
        this.boxes.add(box);
    }

    public void removeBox(Box box) {
        this.boxes.remove(boxes.indexOf(box));
    }
}
